# Changelog
All notable changes to this project made by Monade Team are documented in this file. For info refer to team@monade.io

## [0.1.0] - 2019-12-16
### Add
- All existing support files
- Setup README, CHANGELOG and repo settings
