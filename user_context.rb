# frozen_string_literal: true

module UserContext
  def with_user(role, company_roles = [])
    context "with user #{role}, roles: #{company_roles.join(', ')}" do
      create_user_with_role(role, company_roles)

      yield
    end
  end

  def create_user_with_role(role, company_roles = [])
    let!(:user) do
      user = create(role)
      user.company = create(:company, roles: company_roles) if company_roles.any?
      sign_in(user)
      user
    end
  end
end
