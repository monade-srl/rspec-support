# RSPEC support folder by monade.io

A set of useful support files for RSPEC

To use it, just add this repo as a submodule:

```bash
git submodule add https://gitlab.com/monade-srl/rspec-support.git ./spec/support/monade
```

If the submodule already exists in the project, run:
```bash
git submodule update
```


## Contributing
Setup git-flow and follow the styleguide. Please keep the CHANGELOG.md file up-to-date.
